import expect from 'expect';
import reducer from '../../src/reducers/ajax-reducer';
import {
    AJAX_SUCCESS,
    AJAX_FAILURE,
    AJAX_PROGRESS,
    AJAX_COMPLETE
} from '../../src/actions/types';


describe('AJAX reducer', () => {
  it('should return the initial state', () => {
    const expectedState = {};
    expect(reducer(undefined,{})).toEqual(expectedState);
  });

  it('should handle AJAX_SUCCESS', () => {
    const initialState = {};
    const payload = 'Success';
    const action = {
      type: AJAX_SUCCESS,
      payload
    };
    const expectedState = {
      message: payload,
      inProgress: false,
      isSuccess: true,
      isFailure: false
    };
    expect(reducer(initialState,action)).toEqual(expectedState);
  });

  it('should handle AJAX_FAILURE', () => {
    const initialState = {};
    const payload = 'Failure';
    const action = {
      type: AJAX_FAILURE,
      payload
    };
    const expectedState = {
      message: payload,
      inProgress: false,
      isSuccess: false,
      isFailure: true
    };
    expect(reducer(initialState,action)).toEqual(expectedState);
  });

  it('should handle AJAX_PROGRESS', () => {
    const initialState = {};
    const payload = 'Progress';
    const action = {
      type: AJAX_PROGRESS,
      payload
    };
    const expectedState = {
      message: payload,
      inProgress: true,
      isSuccess: false,
      isFailure: false
    };
    expect(reducer(initialState,action)).toEqual(expectedState);
  });

  it('should handle AJAX_COMPLETE', () => {
    const initialState = {};
    const payload = null;
    const action = {
      type: AJAX_COMPLETE,
      payload
    };
    const expectedState = {
      message: payload,
      inProgress: false,
      isSuccess: false,
      isFailure: false
    };
    expect(reducer(initialState,action)).toEqual(expectedState);
  });
});
