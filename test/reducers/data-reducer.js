import expect from 'expect';
import reducer from '../../src/reducers/data-reducer';
import {
    SAVE_DATA_LOCAL,
    SAVE_DATA_SERVER,
    GET_DATA_LOCAL
} from '../../src/actions/types';


describe('DATA reducer', () => {

  it('should return the initial state', () => {
    const expectedState = { excelData: null };
    expect(reducer(undefined,{})).toEqual(expectedState);
  });


  it('should handle SAVE_DATA_LOCAL', () => {
    const initialState = {};
    const payload = [
      {
        id: 1,
        fileName: 'file1',
        data: []
      },
      {
        id: 2,
        fileName: 'file2',
        data: []
      }
    ];
    const action = {
      type: SAVE_DATA_LOCAL,
      payload
    };
    const expectedState = {
      excelData: payload
    };

    expect(reducer(initialState,action)).toEqual(expectedState);
  });

  it('should handle SAVE_DATA_SERVER', () => {
    const initialState = {};
    const payload = [
      {
        id: 1,
        fileName: 'file1',
        data: []
      },
      {
        id: 2,
        fileName: 'file2',
        data: []
      }
    ];
    const action = {
      type: SAVE_DATA_SERVER,
      payload
    };
    const expectedState = {
      excelData: payload
    };

    expect(reducer(initialState,action)).toEqual(expectedState);
  });

  it('should handle GET_DATA_LOCAL', () => {
    const initialState = {};
    const payload = [
      {
        id: 1,
        fileName: 'file1',
        data: []
      },
      {
        id: 2,
        fileName: 'file2',
        data: []
      }
    ];
    const action = {
      type: GET_DATA_LOCAL,
      payload
    };
    const expectedState = {
      excelData: payload
    };
    
    expect(reducer(initialState,action)).toEqual(expectedState);
  });
  
});
