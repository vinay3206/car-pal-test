import expect from 'expect';
import React from 'react';
import { shallow } from 'enzyme';

import { SyncToServer } from '../..//src/components/sync-to-server';


function setup() {
  const enzymeWrapper = shallow(<SyncToServer/>);
  return enzymeWrapper;
}

describe('Component SyncToServer', () => {

    it('Button is rendered', () => {
      const enzymeWrapper = setup();
      expect(enzymeWrapper.find('.btn').length).toEqual(1);
    });

    it('Button is enabled when network is active', () => {
      const enzymeWrapper = setup();
      enzymeWrapper.setState({
        isOnline: true
      })
      expect(enzymeWrapper.find('.btn').props().disabled).toEqual(false);
    });

    it('Button is disabled when network is active', () => {
      const enzymeWrapper = setup();
      enzymeWrapper.setState({
        isOnline: false
      })
      expect(enzymeWrapper.find('.btn').props().disabled).toEqual(true);
    });
});
