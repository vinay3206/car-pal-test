 import expect from 'expect';
import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import { Link } from 'react-router';

import { FileList } from '../../src/components/file-list';

const data = [ {fileName: 'file1', data: [], id: 1 }, { fileName: 'file2', data: [], id: 2 }];

function setup() {
  const enzymeWrapper = shallow(<FileList getDataFromLocal={sinon.stub()} data={data}/>);
  return enzymeWrapper;
}

describe('Component FileList', () => {

    it('Page Header is rendered', () => {
      const enzymeWrapper = setup();
      expect(enzymeWrapper.find('.page-header').first().text()).toEqual('Records');
    });

    it('Table is rendered', () => {
      const enzymeWrapper = setup();
      expect(enzymeWrapper.find('table').length).toEqual(1);
    });

    it('tr is rendered', () => {
      const enzymeWrapper = setup();
      expect(enzymeWrapper.find('tr').length).toEqual(3);
    });

    it('Link is rendered', () => {
      const enzymeWrapper = setup();
      expect(enzymeWrapper.find(Link).length).toEqual(2);
    });

    it('Link first point to first file', () => {
      const enzymeWrapper = setup();
      expect(enzymeWrapper.find(Link).first().props().to).toEqual('/file/1');
    });

    it('Link second point to first file', () => {
      const enzymeWrapper = setup();
      expect(enzymeWrapper.find(Link).at(1).props().to).toEqual('/file/2');
    });

});
