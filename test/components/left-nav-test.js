import expect from 'expect';
import React from 'react';
import { shallow } from 'enzyme';
import { Link } from 'react-router';

import LeftNavigation from '../../src/components/left-nav';
import SyncToServer from '../..//src/components/sync-to-server';


function setup() {
  const enzymeWrapper = shallow(<LeftNavigation/>);
  return enzymeWrapper;
}

describe('Component LeftNavigation', () => {

    it('should contain 3 Link Component', () => {
      const enzymeWrapper = setup();
      expect(enzymeWrapper.find(Link).length).toEqual(3);
    });

    it('should contain Link to base route', () => {
      const enzymeWrapper = setup();
      expect(enzymeWrapper.find(Link).first().props().to).toEqual('/');
    });

    it('should contain Link to files route', () => {
      const enzymeWrapper = setup();
      expect(enzymeWrapper.find(Link).at(1).props().to).toEqual('/files');
    });

    it('should contain Link to upload route', () => {
      const enzymeWrapper = setup();
      expect(enzymeWrapper.find(Link).at(2).props().to).toEqual('/upload');
    });

    it('should contain SyncToServer Component', () => {
      const enzymeWrapper = setup();
      expect(enzymeWrapper.find(SyncToServer).length).toEqual(1);
    });

});
