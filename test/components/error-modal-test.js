import expect from 'expect';
import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import { ErrorModal } from '../../src/components/error-modal';

const errorMessage = 'Something went wrong';
var onButtonClick = sinon.spy();

function setup() {
  const enzymeWrapper = shallow(<ErrorModal message={errorMessage} hideModal={onButtonClick}/>);
  return enzymeWrapper;
}

describe('Component ErrorModal', () => {

    it('should render text Error', () => {
      const enzymeWrapper = setup();
      const text = 'Error!';
      expect(enzymeWrapper.find('.color-error').text()).toEqual(text);
    });

    it('should render error message', () => {
      const enzymeWrapper = setup();
      expect(enzymeWrapper.find('.alert-danger').text()).toEqual(errorMessage);
    });

    it('simulates click events', () => {
      const enzymeWrapper = setup();
      enzymeWrapper.find('button').simulate('click');
      expect(onButtonClick.callCount).toEqual(1);
    });

});
