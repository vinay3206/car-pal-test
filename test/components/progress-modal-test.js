import expect from 'expect';
import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import { ProgressModal } from '../../src/components/progress-modal';
import {
  ModalTitle
} from 'react-modal-bootstrap';

const progressMessage = 'In Progress';

function setup() {
  const enzymeWrapper = shallow(<ProgressModal message={progressMessage}/>);
  return enzymeWrapper;
}

describe('Component Progress', () => {

    it('should render progress message', () => {
      const enzymeWrapper = setup();
      expect(enzymeWrapper.find(ModalTitle).length).toEqual(1);
    });

    it('should render progress div', () => {
      const enzymeWrapper = setup();
      expect(enzymeWrapper.find('.progress-bar').length).toEqual(1);
    });

});
