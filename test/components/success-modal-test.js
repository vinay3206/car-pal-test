import expect from 'expect';
import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import { SuccessModal } from '../../src/components/success-modal';

const successMessage = 'Call Success';
var onButtonClick = sinon.spy();

function setup() {
  const enzymeWrapper = shallow(<SuccessModal message={successMessage} hideModal={onButtonClick}/>);
  return enzymeWrapper;
}

describe('Component SuccessModal', () => {

    it('should render text Success', () => {
      const enzymeWrapper = setup();
      const text = 'Success!';
      expect(enzymeWrapper.find('.color-success').text()).toEqual(text);
    });

    it('should render success message', () => {
      const enzymeWrapper = setup();
      expect(enzymeWrapper.find('.alert-success').text()).toEqual(successMessage);
    });

    it('simulates click events', () => {
      const enzymeWrapper = setup();
      enzymeWrapper.find('button').simulate('click');
      expect(onButtonClick.callCount).toEqual(1);
    });

});
