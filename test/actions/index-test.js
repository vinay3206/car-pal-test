import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from '../../src/actions/index';
import * as types from '../../src/actions/types';
import expect from 'expect';
import sinon from 'sinon';
import localforage from 'localforage';


const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const dataObj = 
{   
    fileName: 'file1',
    data: [ { row1: 'row1'}, { row2: 'row2' }]
}

describe('Actions', () => {

    beforeEach(()=> {
        sinon.stub(localforage, 'getItem').callsFake(() => {
            return new Promise((resolve) => {
                resolve([
                    dataObj
                ])
            });
        });

        sinon.stub(localforage, 'setItem').callsFake(() => {
            return new Promise((resolve) => {
                resolve([
                    dataObj
                ])
            });
        });
    });

    afterEach(() => {
        localforage.getItem.restore();
        localforage.setItem.restore();
    });

    it('Function:saveDataToLocal | creates SAVE_DATA_LOCAL and AJAX_PROGRESS and AJAX_SUCCESS when request is passed to save data to local', () => {
        const requestObj = dataObj
        const expectedAction = [
            {
                type: types.AJAX_PROGRESS,
                payload: 'Saving Data to browser storage'
            },
            {
                type: types.SAVE_DATA_LOCAL,
                payload: [{
                    fileName: requestObj.fileName,
                    data: requestObj.data
                }]
            },
            {
                type: types.AJAX_SUCCESS,
                payload: 'Data saved to browser storage'
            }
        ];
        const store = mockStore({});
    
        return store.dispatch(actions.saveDataToLocal(requestObj))
            .then(() => {
                expect(store.getActions())
                    .toEqual(expectedAction);
            })
    });

    it('Function:getDataFromLocal | creates GET_DATA_LOCAL when method called', () => {
        const expectedAction = [
            {
                type: types.GET_DATA_LOCAL,
                payload: [{
                    fileName: dataObj.fileName,
                    data: dataObj.data
                }]
            }
        ];
        const store = mockStore({});

        return store.dispatch(actions.getDataFromLocal())
            .then(() => {
                expect(store.getActions())
                    .toEqual(expectedAction);
            });
    });

    it('Function:saveDataToServer | creates ajaxProgress when email is not yet confirmed', () => {
        
        const expectedAction = [
            {
                type: types.AJAX_PROGRESS,
                payload: 'Saving Data to server'
            },
            {
                type: types.AJAX_PROGRESS,
                payload: 'Saving these files to server: file1'
            },
            {
                type: types.AJAX_SUCCESS,
                payload: 'Saved to server'
            }
        ];
        const store = mockStore({});
    
        return store.dispatch(actions.saveDataToServer())
            .then(() => {
                setTimeout(() => {
                    expect(store.getActions())
                    .toEqual(expectedAction);
                },1000);
            })
    });
});
