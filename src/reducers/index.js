import { combineReducers } from 'redux';
import data from './data-reducer';
import ajaxReducer from './ajax-reducer';


/**
 * [rootReducer this method combines all the reducers and returns one reducer]
 *
 */
const rootReducer = combineReducers({
	data,
	ajax: ajaxReducer,
});


export default rootReducer;