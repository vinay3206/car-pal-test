import {
    AJAX_PROGRESS,
    AJAX_SUCCESS,
    AJAX_FAILURE,
    AJAX_COMPLETE
} from '../actions/types';

/**
 * [this method is responsible for changing the state of ajax]
 * @param  {Object} state  [initial state which is passed]
 * @param  {[Object]} action [action object which has a payload which needs to update the state]
 * @return {[String]}        [action has a type which is used to determine the action performed]
 */
export default (state = {}, action) => {
    switch (action.type) {
        case AJAX_SUCCESS:
            return {
                ...state,
                message: action.payload,
                inProgress: false,
                isSuccess: true,
                isFailure: false
            };
        case AJAX_FAILURE:
            return {
                ...state,
                message: action.payload,
                inProgress: false,
                isSuccess: false,
                isFailure: true
            };
        case AJAX_PROGRESS:
            return {
                ...state,
                message: action.payload,
                inProgress: true,
                isSuccess: false,
                isFailure: false
            };
        case AJAX_COMPLETE:
            return {
                ...state,
                message: null,
                inProgress: false,
                isSuccess: false,
                isFailure: false
            };
        default:
            return state;
    }

    return state;
}
