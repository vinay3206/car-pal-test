import {
    SAVE_DATA_LOCAL,
    SAVE_DATA_SERVER,
    GET_DATA_LOCAL
} from '../actions/types';

export const initialState = {
    excelData: null
};

/**
 * [dataReducer this method is responsible for updating the state for data]
 * @param  {[Object]} state  [inital state object]
 * @param  {[Object]} action [current dispatched action]
 * @return {[String]}        [type of dispatched action]
 */
export default function dataReducer(state = initialState, action) {
    switch(action.type) {
        case SAVE_DATA_LOCAL:
            return { ...state, excelData: action.payload };
        case SAVE_DATA_SERVER:
            return { ...state, excelData: action.payload };
        case GET_DATA_LOCAL:
            return {...state, excelData: action.payload }
        default:
            return state;
    }
}
