import { createStore, applyMiddleware, compose } from 'redux';
import reduxThunk from 'redux-thunk';

import reducers from '../reducers';

/**
 * [configureStore creates store for the react app with reducers]
 * @return {[Object]} [returns store object]
 */
function configureStore() {
    return createStore(
        reducers,
        {},
        compose(
            applyMiddleware(
                reduxThunk
            ),
            window.devToolsExtension ? window.devToolsExtension() : f => f
        )
    );
}

export default configureStore;