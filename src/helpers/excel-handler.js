import XLSX from 'xlsx';

export default class ExcelHandler
{
	/**
	 * this method is responsible for converting excel data to JSON object.
	 * It used xlsx.js to parse the workbook and sheets and generates a JSON object
	 */
	toJSON(file, callback)
	{
		const reader = new FileReader();

	    reader.onload = function(e){
	        const data = e.target.result;
	        const workbook = XLSX.read(data, {type : 'binary'});
	        let result = [];

	        workbook.SheetNames.forEach(function(sheetName){
	            // Here is your object
	            const rowObject = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
	            result = [...result, ...rowObject];
	        });
	        callback(null,result);
	    };

	    reader.onerror = function(ex){
	        callback(ex,null);
	    };

	    reader.readAsBinaryString(file);
	}
}