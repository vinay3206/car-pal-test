import React, { Component } from 'react';
import {
  Modal,
  ModalHeader,
  ModalTitle,
  ModalClose,
  ModalBody,
  ModalFooter
} from 'react-modal-bootstrap';
import { connect } from 'react-redux';
import * as actions from '../actions';


export class SuccessModal extends Component {

  constructor() {
      super();
      this.hideModal = this.hideModal.bind(this);
  }

  /**
   * [hideModal this method hides the modal window]
   */
  hideModal() {
    this.props.hideModal();
  }

  render() {
    return (
      <Modal isOpen={this.props.isOpen}>
          <ModalHeader>
            <ModalTitle><span className="color-success">Success!</span></ModalTitle>
          </ModalHeader>
          <ModalBody>
            <div className="alert alert-success" role="alert">{this.props.message}</div>
          </ModalBody>
          <ModalFooter>
            <button className='btn btn-default background-success' onClick={this.hideModal}>
              Close
            </button>
          </ModalFooter>
     </Modal>
    );
  }
};

/**
 * [mapStateToProps this method maps the state props to the component]
 * @param  {[Object]} state [recived current state object]
 */
function mapStateToProps(state) {
    return {
        isOpen: state.ajax.isSuccess,
        message: state.ajax.message
    };
}

export default connect(mapStateToProps,actions)(SuccessModal);
