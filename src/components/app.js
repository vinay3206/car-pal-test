import React, { Component } from 'react';

import LeftNavigation from './left-nav';
import ProgressModal from './progress-modal';
import ErrorModal from './error-modal';
import SuccessModal from './success-modal';


export default class App extends Component {
  // this is the main wrapper app which is used to display different component whenever route changes.
  // it also renders LeftNavigation and Modal components which are common and used accross different routes 
  render() {
    return (
        	<div className="row full-width">

        		<div className="col-md-2">
        			<LeftNavigation />
        		</div>
        		<div className="col-md-10">
            		{this.props.children}
        		</div>
                <ProgressModal />
                <ErrorModal />
                <SuccessModal />
        	</div>
    );
  }
}
