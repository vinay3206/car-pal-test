import React, { Component } from 'react';
import { Link } from 'react-router';
import SyncToServer from './sync-to-server';

// this component handles the left side navigation
// It also has a Sync button which is used to sync data to the server
export default class LeftNavigation extends Component {
  render() {
    return (
        <div className="navbar navbar-inverse navbar-fixed-left">
         <Link className="navbar-brand" to="/">Car Pal Test</Link>
		  	 <ul className="nav navbar-nav">
  			   <li><Link to="/files">Files</Link></li>
  			   <li><Link to="/upload">Upload</Link></li>
		  	</ul>
        <SyncToServer />
		</div>
    );
  }
}