import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../actions';


export class SyncToServer extends Component {

  constructor(props)
  {
    super(props);

    this.updateIndicator = this.updateIndicator.bind(this);
    this.state = { isOnline : true };
  }

  /**
   * [updateIndicator this function just updated the state with the navigator.onLine property]
   * @return {[undefined]} [none]
   */
  updateIndicator(){
    this.setState({
        isOnline: navigator.onLine
    });
  }

  /**
   * Setting online and offline event listners to be called on component mount
   */
  componentDidMount()
  {
    window.addEventListener('online',  this.updateIndicator);
    window.addEventListener('offline', this.updateIndicator);
    this.updateIndicator();
  }

  /**
   * removing the listners when component will be unMounted
   */
  componentWillUnmount()
  {
    window.removeEventListner('online', this.updateIndicator);
    window.removeEventListner('offline', this.updateIndicator);
  }

  render()
  {
    return (
        // button is disabled untill unless user is online
        <button className="btn btn-primary margin-top-2" type="button" disabled={!this.state.isOnline} onClick={this.props.saveDataToServer}>
          Sync to Server
          <span className="glyphicon glyphicon-refresh margin-left-1" aria-hidden="true">
          </span>
        </button>
    );
  }
}


export default connect(null,actions)(SyncToServer);
