import React, { Component } from 'react';
import {
  Modal,
  ModalHeader,
  ModalTitle,
  ModalClose,
  ModalBody,
  ModalFooter
} from 'react-modal-bootstrap';
import { connect } from 'react-redux';
import * as actions from '../actions';


export class ErrorModal extends React.Component {

  constructor() {
      super();
      this.hideModal = this.hideModal.bind(this);
  }

  /**
   * [hideModal this method hides the modal window]
   */
  hideModal() {
    this.props.hideModal();
  }

  render() {
    return (
      <Modal isOpen={this.props.isOpen}>
          <ModalHeader className='hello'>
              <ModalTitle><span className='color-error'>Error!</span></ModalTitle>
          </ModalHeader>
          <ModalBody>
            <div className="alert alert-danger" role="alert">{this.props.message}</div>
          </ModalBody>
          <ModalFooter>
            <button className='btn btn-default background-error' onClick={this.hideModal}>
              Close
            </button>
          </ModalFooter>
     </Modal>
    );
  }
};

function mapStateToProps(state) {
    return {
        isOpen: state.ajax.isFailure,
        message: state.ajax.message
    };
}

export default connect(mapStateToProps,actions)(ErrorModal);
