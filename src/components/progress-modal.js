import React, { Component } from 'react';
import {
  Modal,
  ModalHeader,
  ModalTitle,
  ModalClose,
  ModalBody,
  ModalFooter
} from 'react-modal-bootstrap';
import { connect } from 'react-redux';


// progess modal component is used to show the progress state of an ajax request
export class ProgressModal extends React.Component {
  render() {
    return (
      <Modal isOpen={this.props.isOpen}>
          <ModalHeader>
            <ModalTitle>{this.props.message}</ModalTitle>
          </ModalHeader>
          <ModalBody>
            <div className="progress progress-bar-info">
              <div className="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                <span className="sr-only">45% Complete</span>
              </div>
            </div>
          </ModalBody>
     </Modal>
    );
  }
};

/**
 * [mapStateToProps this method maps the state props to the component]
 * @param  {[Object]} state [recived current state object]
 */
function mapStateToProps(state) {
    return {
        isOpen: state.ajax.inProgress,
        message: state.ajax.message
    };
}

export default connect(mapStateToProps)(ProgressModal);
