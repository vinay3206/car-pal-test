import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

import * as actions from '../actions';



export class FileList extends Component {


  constructor(props) {
    super(props);

    this.renderRecords = this.renderRecords.bind(this);
    this.renderTableBody = this.renderTableBody.bind(this);
  }

  // this method is responsible for rending the different records
  // it check for data first and if it is not there it makes a request
  // it checks for data again where is is an empty list or not
  renderRecords() {
    if(!this.props.data)
    {
        this.props.getDataFromLocal();
    }
    else if(!this.props.data.length)
    {
        return <div className="alert alert-info" role="alert">Heads up! No data to show. Please upload some <Link to="/upload">Excel Files</Link></div>
    }
    else
    {
      return (
        <div className="col-md-12">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>ID</th>
                <th>Filename</th>
                <th>Link</th>
               </tr> 
            </thead>
            <tbody>
              {this.renderTableBody()}
            </tbody>
          </table>
        </div>
      );
    }
  }

  /**
   * [renderTableBody this method renders table tr tags which are repated for each data]
   */
  renderTableBody() {
    return this.props.data.map((d) => {
      return( <tr key={d.id}>
                <td>{d.id}</td>
                <td>{d.fileName}</td>
                <td><Link to={'/file/'+ d.id}>View/Edit</Link></td>
              </tr>
      );
    })
    return (<tr></tr>)
  }

  render() {
    return (
              <div>
                <div className="row">
                  <h1 className="page-header">
                        Records
                    </h1>
                </div>    
                <div className="row margin-top-4">
                    <div className="col-md-12">
                        {this.renderRecords()}
                    </div>
                </div>    
          </div>
    );
  }
}

function mapStateToProps(state) {
    return {
        data: state.data.excelData
    };
}

export default connect(mapStateToProps, actions)(FileList);

