import React, { Component, PropTypes } from 'react';
import ReactDataGrid from 'react-data-grid';

import { connect } from 'react-redux';
import * as actions from '../actions';



export class Grid extends Component {


  constructor(props) {
    super(props);

    this.getColumns = this.getColumns.bind(this);
    this.rowGetter = this.rowGetter.bind(this);
    this.handleGridRowsUpdated = this.handleGridRowsUpdated.bind(this);
    this.onSaveData = this.onSaveData.bind(this);

    // setting initial state to the data passed through props
    this.state = { rows: props.data };
  }

  /**
   * Updating state whenever props values are changed.
   * Will be called when new data passed through props is changed.
   */
  componentWillReceiveProps(nextProps)
  {
    this.setState({ 
        rows: nextProps.data
    });
  }

  /**
   * [onSaveData this method is called on click of Save data btn and it sends an action to save the data to local]
   * @return {[type]} [description]
   */
  onSaveData(){
    this.props.saveDataToLocal({
      fileName: this.props.fileName,
      data: this.state.rows
    })
  }

  /*
  this method is responsible for generating the columns which will be used as headers in the React Grid component
   */
  getColumns() {

    const firstObject = this.state.rows[0];
    const keys = Object.keys(firstObject);

    return keys.map((key) => {
        return {
            key: key,
            name: key,
            editable: true
        }
    });
  }


  /**
  this method is called by React-Data-Grid comonent to get the actual row data.
   */
  rowGetter(i) {
    return this.state.rows[i];
  }

  /**
   * [handleGridRowsUpdated this method is called whenever grid data is updated abd changes needs to be synched to original state]
   * @param  {[Number]} options.fromRow [its the row which is changed]
   * @param  {[type]} options.toRow   [row number till which changes are done]
   * @param  {[Object]} options.updated [Object/property which is updated]
   */
  handleGridRowsUpdated({ fromRow, toRow, updated }) {
    const rows = this.state.rows;
    const updatedKeys = Object.keys(updated);

    updatedKeys.forEach((key) => {
        rows[fromRow][key] = updated[key];
    });
    this.setState({
        rows
    })
  }

  render() {
    return (
      <div className="row">
          <ReactDataGrid
            enableCellSelect={true}
            rowGetter={this.rowGetter}
            columns={this.getColumns()}
            rowsCount={this.state.rows.length}
            minHeight={500}
            onGridRowsUpdated={this.handleGridRowsUpdated}
           />
           <div className="margin-top-2 text-center">
                <button type="button" className="btn btn-primary btn-lg" onClick={this.onSaveData}>Save Data</button>
           </div>
      </div>     
    );
  }
}


// Setting prop types for component here
Grid.propTypes = {
    fileName: PropTypes.string.isRequired,
    data: PropTypes.array.isRequired
};


export default connect(null, actions)(Grid);
