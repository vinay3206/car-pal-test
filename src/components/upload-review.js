import React, { Component } from 'react';


import FileInput from 'react-file-input';
import ExcelHandler from '../helpers/excel-handler';
import Grid from './grid';



export default class UploadAndReview extends Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {data: []};
  }

  /**
   * [handleChange this method gets called whenever a new Excel file is uploaded]
   * @param  {[Object]} event [Event Object]
   * @return {[undefined]}       [nothing]
   */
  handleChange(event) {
    const excelHandler = new ExcelHandler();
    const fileName = event.target.files[0].name;

    // call to excelHandler here and passed a callback function to get the response.
    // once JSON is recived the component will update its own state and Grid will be shown
    excelHandler.toJSON(event.target.files[0],function(err,json){
        if(!err){
            this.setState({ data: json, fileName:fileName });
        }
    }.bind(this));
  }

  render() {
    return (
        <div>
            <div className="row">
            	<h1 className="page-header">
                    Upload and Review File
                </h1>
                <div className="text-right">
                    <label className="btn btn-info">
                        <form>
                            <FileInput name="myFile"
                                       accept=".xlsx,.xls"
                                       placeholder=""
                                       className="hide"
                                       onChange={this.handleChange} />
                        </form>
                        Upload Excel
                        <span className="glyphicon glyphicon-upload margin-left-1" aria-hidden="true"></span>
                    </label>
                </div>
            </div>    
            <div className="row margin-top-4">
                <div className="col-md-12">
                    {this.state.data.length > 0 &&
                        <Grid data={this.state.data} fileName={this.state.fileName} />
                    }
                </div>
            </div>    
            
        </div>
    );
  }
}
