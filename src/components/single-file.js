import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

import * as actions from '../actions';
import Grid from './grid';


class SingleFile extends Component {


  constructor(props) {
    super(props);
  }

  // render method checks weather data is already loaded or not
  // if not loaded then it will call the getDataFromLocal method and then component will be re rendered again with actual data
  render() {
    if(!this.props.data)
    {
      this.props.getDataFromLocal();
      return (<div className="alert alert-info" role="alert">Loading Data </div>);
    }
    else
    {
        const fileData = this.props.data.find(d => d.id == this.props.params.id);
        return (
          <div>
            <div className="row">
              <h1 className="page-header">
                {fileData.fileName}
              </h1>
              <Link to="/files">Back</Link>
            </div>    
            <div className="row margin-top-4">
                <div className="col-md-12">
                    { fileData.data.length > 0 &&
                      <Grid data={fileData.data} fileName={fileData.fileName}/>
                    }
                </div>
            </div>     
          </div>
        );
    }
  }

}

/**
 * [mapStateToProps this method maps the state props to the component]
 * @param  {[Object]} state [recived current state object]]
 */
function mapStateToProps(state) {
    return {
        data: state.data.excelData
    };
}

export default connect(mapStateToProps, actions)(SingleFile);

