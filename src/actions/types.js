// data actions
export const SAVE_DATA_LOCAL = 'save_data_local';
export const SAVE_DATA_SERVER = 'save_data_server';
export const GET_DATA_LOCAL = 'get_data_local';

// modal actions
export const AJAX_PROGRESS = 'ajax_progress';
export const AJAX_SUCCESS = 'ajax_success';
export const AJAX_FAILURE = 'ajax_failure';
export const AJAX_COMPLETE = 'ajax_complete';