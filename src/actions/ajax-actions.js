import {
    AJAX_PROGRESS,
    AJAX_COMPLETE,
    AJAX_SUCCESS,
    AJAX_FAILURE
} from './types';

function ajaxFailure(error) {
    return {
        type: AJAX_FAILURE,
        payload: error
    };
}

function ajaxSuccess(success) {
    return {
        type: AJAX_SUCCESS,
        payload: success
    };
}

function ajaxProgress(message) {
    return {
        type: AJAX_PROGRESS,
        payload: message
    }
}

function ajaxComplete() {
    return {
        type: AJAX_COMPLETE,
        payload: null,
    }
}

function hideModal() {
    return dispatch => {
        dispatch({
            type: AJAX_COMPLETE,
            payload: null
        });
    }
}

export {
    ajaxFailure,
    ajaxSuccess,
    ajaxProgress,
    ajaxComplete,
    hideModal
};
