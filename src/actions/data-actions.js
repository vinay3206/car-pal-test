import {
    SAVE_DATA_LOCAL,
    SAVE_DATA_SERVER,
    GET_DATA_LOCAL,
} from './types';

import {
    ajaxFailure,
    ajaxSuccess,
    ajaxProgress,
    ajaxComplete,
    hideModal
} from './ajax-actions';


import localforage from 'localforage';

const LOCAL_STORAGE_KEY = 'car-pal-test';

function saveDataToLocal({fileName, data}) {
    return dispatch => {
        dispatch(ajaxProgress('Saving Data to browser storage'));
        return localforage.getItem(LOCAL_STORAGE_KEY).then((value) =>{
        	value = value || [];
        	const record = value.find(v => v.fileName === fileName);

        	if(record)
        	{
        		record.data = data;
        	}
        	else
        	{
	        	value.push({
	        		id: value.length + 1,
	        		fileName,
	        		data
	        	});
	       	}
        	localforage.setItem(LOCAL_STORAGE_KEY, value);
        	dispatch({
        		type: SAVE_DATA_LOCAL,
        		payload: value
        	});
            dispatch(ajaxSuccess('Data saved to browser storage'));
        });
    }
}

function saveDataToServer()
{
    return dispatch => {
        dispatch(ajaxProgress('Saving Data to server'));
        return localforage.getItem(LOCAL_STORAGE_KEY).then((values) => {
            const fileNames = values.map( v => v.fileName).join(',');
            dispatch(ajaxProgress(`Saving these files to server: ${fileNames}`));
            setTimeout(() => {
                dispatch(ajaxSuccess("Saved to server"));
            },1000);
        });
    }
}

function getDataFromLocal()
{
	return dispatch => {
		return localforage.getItem(LOCAL_STORAGE_KEY).then((value) => {
			value = value || [];
			dispatch({
				type: GET_DATA_LOCAL,
				payload: value
			});
		});
	}
}

function getDataFromServer()
{

}

export {
	saveDataToLocal,
	saveDataToServer,
	getDataFromLocal,
	getDataFromServer
}