import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore} from 'redux';
import { Router, Route, IndexRoute, IndexRedirect, useRouterHistory } from 'react-router';

// components
import App from './components/app';
import UploadAndReview from './components/upload-review';
import FileList from './components/file-list';
import SingleFile from './components/single-file';


import configureStore from './store';


import { createHistory } from 'history';

// store object to be passed to the router
const Store = configureStore();

const appHistory = useRouterHistory(createHistory)({ basename: '/' });

class AppProvider extends Component {

	render() {
        // setting up different routes of the application
        return (
            <Provider store={Store}>
                <Router history={appHistory}>
                    <Route path='/' component={App}>
                        <IndexRedirect to='/files' />
                    	<Route path='upload' component={UploadAndReview} />
                        <Route path='files' component= {FileList} />
                        <Route path='file/:id' component= {SingleFile} />
                    </Route>
                </Router>
            </Provider>
        );
    }
}


// attching app to the dom here
ReactDOM.render(
  <AppProvider />
  , document.querySelector('.app'));
