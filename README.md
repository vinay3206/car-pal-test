# CarPal-Test

###Getting Started###

You Need to have node js installed to run this project.

Project is built on ReactJS and it used React-redux for state management in the app.

For tests it uses enzyme to test React components and Mocha to run the tests.

####Familiar with Git?#####
Checkout this repo, install dependencies, then start the process with the following:

```
	> git clone https://gitlab.com/vinay3206/car-pal-test.git
	> cd car-pal-test
	> npm install
	> npm start
	> Open "localhost:8080" in your browser
```

####To Run tests#####

```
	> npm test
```
